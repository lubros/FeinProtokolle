\babel@toc {ngerman}{}
\contentsline {section}{\numberline {1}Begr\IeC {\"u}\IeC {\ss }ung}{2}
\contentsline {section}{\numberline {2}Feststellung der Beschlussf\IeC {\"a}higkeit}{2}
\contentsline {section}{\numberline {3}Bericht des 1. und 2. Vorstands}{2}
\contentsline {section}{\numberline {4}Bericht des Kassierers und der Kassenpr\IeC {\"u}fer}{2}
\contentsline {section}{\numberline {5}Entlastung des 1. und 2. Vorstands}{2}
\contentsline {section}{\numberline {6}Entlastung des Kassierers}{3}
\contentsline {section}{\numberline {7}Neuwahl des 1. und 2. Vorstands}{3}
\contentsline {section}{\numberline {8}Neuwahl des Kassierers}{4}
\contentsline {section}{\numberline {9}Neuwahl des \IeC {\"O}ffentlichkeitsbeauftragten}{4}
\contentsline {section}{\numberline {10}Neuwahl des Schriftf\IeC {\"u}hrers}{4}
\contentsline {section}{\numberline {11}Festlegung eines Termins zur Planung der T\IeC {\"a}tigkeiten im WiSe 18/19}{4}
\contentsline {section}{\numberline {12}Sonstiges}{5}
\contentsline {section}{\numberline {13}Anwesenheit}{6}
